(function($) {
  
  Drupal.behaviors.timerField = {
    
    timers : [],
    
    attach : function() {
      $('.timer-field').each(function() {
        if(Drupal.settings.timer.timer_access) {
          Drupal.behaviors.timerField.addControlLinks($(this));
        }
        else {
          $(this).attr('disabled', 'disabled');          
          if($(this).hasClass('timer-field-autostart')) {
            Drupal.behaviors.timerField.startTimer('#' + $(this).attr('id'));
          }
        }
      });
    },
    addControlLinks : function(element) {
      var $controlLink = $('<a>').attr('href', '#' + element.attr('id'))
                                 .addClass('timer-link')
                                 .html(Drupal.t('Start timer'))
                                 .click(Drupal.behaviors.timerField.timerFieldLinkClick);
      element.after($controlLink);
      if(element.hasClass('timer-field-autostart')) {
        $controlLink.trigger('click');
      }
    },
    timerFieldLinkClick : function(link) {
      if($(this).hasClass('timer-started')) {
        $(this).removeClass('timer-started')
               .html(Drupal.t('Start timer'));;
        Drupal.behaviors.timerField.stopTimer($(this).attr('href'));
      }
      else {
        $(this).addClass('timer-started')
               .html(Drupal.t('Stop timer'));
        Drupal.behaviors.timerField.startTimer($(this).attr('href'));
      }
      return false;
    },
    startTimer : function(id) {
      Drupal.behaviors.timerField.timers[id] = id;
      Drupal.behaviors.timerField.countTimer(id);
    },
    stopTimer : function(id) {
      Drupal.behaviors.timerField.timers[id] = 0;
    },
    countTimer : function(id) {
      if(!Drupal.behaviors.timerField.timers[id]) {
        return;
      }
      $(id).val(Drupal.behaviors.timerField.getTime($(id).val()));
            
      setTimeout('Drupal.behaviors.timerField.countTimer("' + id + '");', 1000);
    },
    getTime : function(time) {
      var finalTime = {};
      time = time.split(':');
      $.each(time, function(index, value) {
        time[index] = parseInt(value, 10);
      });
      if(time.length == 3) {
        seconds = time[2] + (time[1] * 60) + (time[0] * (60 * 60));
      }
      if(time.length == 2) {
        seconds = time[1] + (time[0] * 60);
      }
      if(time.length == 1) {
        seconds = time[0];
      }
      seconds = parseInt(seconds, 10) + 1;
      finalTime.hour = Math.floor(seconds / (60 * 60));
      finalTime.minute = Math.floor((seconds - (finalTime.hour * 60 * 60))/ 60);
      finalTime.seconds = seconds - (finalTime.hour * 60 * 60) - (finalTime.minute * 60);
      $.each(finalTime, function(index, value) {
        finalTime[index] = (value.toString().length == 1) ? '0' + value.toString() : value;
      });
      if(parseInt(finalTime.hour, 10)) {
        return finalTime.hour + ':' + finalTime.minute + ':' + finalTime.seconds;
      }
      else {
        return finalTime.minute + ':' + finalTime.seconds;
      }
    }
  };
  
})(jQuery);