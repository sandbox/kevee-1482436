<?php


/**
 * @file
 *  Creates a widget to track the editing or creation time of an entity
 */
 
/**
 * Implements hook_field_info().
 */
function timer_field_info() {
  $info = array(
    'timer' => array(
      'label' => t('Timer'),
      'description' => t('Tracks the time it took to create or edit this item.'),
      'translatable' => 0,
      'settings' => array('start_while_editing' => 1, 'auto_start' => 1, 'hide_field' => 0),
      'instance_settings' => array(),
      'default_widget' => 'timer_widget',
      'default_formatter' => 'timer_default',
    ),
  );
  return $info;
}

/**
 * Implements hook_field_settings_form().
 */
function timer_field_settings_form($field, $instance, $has_data) {
  $settings = $field['settings'];
  $form = array();
  $form['start_while_editing'] = array(
    '#type' => 'checkbox',
    '#title' => t('Start timer when editing'),
    '#description' => t('When enabled, if the user is editing exiting content, the timer will continue to add time.'),
    '#default_value' => $settings['start_while_editing'],
  );
  $form['auto_start'] = array(
    '#type' => 'checkbox',
    '#title' => t('Start timer automatically'),
    '#default_value' => $settings['auto_start'],
  );
  $form['hide_field'] = array(
    '#type' => 'checkbox',
    '#title' => t('Hide the timer'),
    '#description' => t('Countdown the timer, but do not display the timer to the user.'),
    '#default_value' => $settings['hide_field'],
  );
  return $form;
}

/**
 * Implements hook_field_widget_info().
 */
function timer_field_widget_info() {
  return $info = array(
    'timer_widget' => array(
      'label' => t('Timer'),
      'field types' => array('timer'),
    ),
  );
}

/**
 * Implements hook_field_widget_form().
 */
function timer_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  drupal_add_js(drupal_get_path('module', 'timer') .'/timer.js');
  drupal_add_css(drupal_get_path('module', 'timer') .'/timer.css');
  drupal_add_js(array('timer' => array('timer_access' => user_access('manually alter timer field'))), 'setting');
  $element += array(
    '#type' => 'textfield',
    '#default_value' => '',
    '#size' => 10,
    '#default_value' => isset($items[$delta]['value']) 
                         ? theme('timer_formatter_default', array('time' => $items[$delta]['value'])) 
                         : 0,
    '#attributes' => array('class' => array('timer-field')),
  );
  if($field['settings']['hide_field']) {
    $element += array(
      '#prefix' => '<div class="element-invisible">',
      '#suffix' => '</div>',
    );
  }
  if($field['settings']['auto_start'] && ($field['settings']['start_while_editing'] || !isset($items[$delta]['value']))) {
    $element['#attributes']['class'][] = 'timer-field-autostart';
  }
  return $element;
}


/**
 * Implements hook_field_is_empty().
 */
function timer_field_is_empty($item, $field) {
  if ($item['value'] === '') {
      return TRUE;
  }
  return FALSE;
}

/**
 * Implements hook_field_formatter_info().
 */
function timer_field_formatter_info() {
  return array(
    'timer_default' => array(
      'label' => t('Timer'),
      'field types' => array('timer'),
      'settings' => array(
        'timer_format' => 'colons',
        'remove_seconds' => FALSE,
      ),
    ),
  );
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function timer_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];
  $element = array();
  
  $element['timer_format'] = array(
    '#type' => 'select',
    '#title' => t('Select format for timer'),
    '#options' => array('colons' => '3:4:45',
                        'words' => '3 hours, 4 minutes, 45 seconds'),
    '#default_value' => $settings['timer_format'],
  );
  
  $element['remove_seconds'] = array(
    '#type' => 'checkbox',
    '#title' => t('Do not show seconds'),
    '#default_value' => $settings['remove_seconds'],
  );
  
  return $element;
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function timer_field_formatter_settings_summary($field, $instance, $view_mode) {
  $timer_display = array('colons' => '3:4:45',
                        'words' => '3 hours, 4 minutes, 45 seconds');
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];
  return t('Displaying timer as @format, @with seconds', array(
            '@format' => $timer_display[$settings['timer_format']],
            '@with' => ($settings['remove_seconds']) ? t('without') : t('with')
          ));
}

/**
 * Implements hook_field_formatter_view().
 */
function timer_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();
  foreach ($items as $item) {
    $element[]['#markup'] = theme('timer_formatter_default', array('time' => $item['value']));
  }
  return $element;
}

function timer_theme() {
  return array(
    'timer_formatter_default' => array(
      'variables' => array('time' => 0, 'format' => 'colons'),
    ),
  );
}

/**
 * Default timer formatter.
 */
function theme_timer_formatter_default($variables) {  
  $time = array();
  $time['hour'] = floor($variables['time'] / (60 * 60));
  $time['minute'] = floor(($variables['time'] - ($time['hour'] * 60 * 60))/ 60);
  $time['seconds'] = $variables['time'] - ($time['hour'] * 60 * 60) - ($time['minute'] * 60);
  if($variables['format'] == 'colons') {
    foreach($time as $k => $v) {
      $time[$k] = str_pad($v, 2, '0', STR_PAD_LEFT);
    }    
    return (intval($time['hour'])) 
             ? $time['hour'] .':'. $time['minute'] .':'. $time['seconds']
             : $time['minute'] .':'. $time['seconds'];
  }
  elseif ($variables['format'] == 'words') {
    return (intval($time['hour'])) 
             ? t('@hours hours, @minute minutes, @seconds seconds', array(
                                                            '@hours' => $time['hour'],
                                                            '@minute' => $time['minute'],
                                                            '@seconds' => $time['seconds']))
             : t('@minute minutes, @seconds seconds', array('@minute' => $time['minute'],
                                                            '@seconds' => $time['seconds']));                                               
  }
}


/**
 * Implements hook_field_validate().
 */
function timer_field_validate($entity_type, $entity, $field, $instance, $langcode, $items, &$errors) {
  foreach ($items as $delta => $value) {
    if(!is_numeric(str_replace(':', '', $value)) || strpos($value, ':') === FALSE) {
      form_set_error($field['field_name'], 'The timer is not in the right format');
    }
  }
}

/**
 * Implements hook_field_presave().
 */
function timer_field_presave($entity_type, $entity, $field, $instance, $langcode, &$items) {
  foreach ($items as $delta => $value) {
    $time = explode(':', $value);
    switch(count($time)) {
      case 1:
        $seconds = $time[0];
        break;
      case 2:
        $seconds = ( $time[0] * 60 ) + $time[1];
        break;
      case 3:
        $seconds = ( $time[0] * 60 * 60 ) + ( $time[1] * 60 * 60 ) + $time[2];
        break;
    }
    $items[$delta] = array('value' => $seconds);
  }
}

/**
 * Implements hook_permissions().
 */
function timer_permissions() {
  return array('manually alter timer field' => array(
    'title' => t('Can start or stop timer fields manually'),
    'description' => t('The user is allowed to change or start/stop timer fields.'),
    ),
  );
}